#!/bin/sh
chmod -R 700 /hiddenservice
mkdir -p /hiddenservice/onion_auth
echo "$TOR_AUTHKEY" > /hiddenservice/onion_auth/wallet.auth_private
/usr/bin/tor -f /etc/tor/torrc
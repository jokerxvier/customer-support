# Tor Auth Setup
1. Execute `./auth.sh` twice to generate 2 keypairs, one for the proxy, one for the wallet.
2. Start both services so they generate their onion hostnames - grab those for later.
3. Proxy setup:
    - `echo "<wallet-hostname-without-.onion>:descriptor:x25519:<proxy-private-key>" >/hiddenservice/onion_auth/wallet.auth_private`
    - `echo "descriptor:x25519:<wallet-public-key>" >/hiddenservice/authorized_clients/wallet.auth`
4. Wallet setup:
    - `echo "<proxy-hostname-without-.onion>:descriptor:x25519:<wallet-private-key>" >/hiddenservice/onion_auth/proxy.auth_private`
    - `echo "descriptor:x25519:<proxy-public-key>" >/hiddenservice/authorized_clients/proxy.auth`
5. Restart Tor containers for changes to take effect

## For example:
```
Wallet:
Hostname: ibfdhqzwbruljahcn4xe5lxhvcq27zxlb3aa7rqhl5enzt4cqnmpfpqd.onion
Private: XCAEOF7DWKJUBW7ZBGCKG67X6ZCF4SJBGJC32OWOEWXPAZDGGVDQ
Public: BSNUTWYLTBA3KVEBK75GNOVNHVXPDJMSN5W7236TDBQJX56J4BZQ

Proxy:
Hostname: l52talc2uiw54cj7wp662dd63gcubqrqfia2uuz3oqzlj2jt2abqhkad.onion
Private: AASXMXOWENXEL4YF2LSUVA34NBKRMICY35QULAMV4OQTVWJ2BVJA
Public: O7NCVDCIYWDHAKQA7QYWYULJWU7GH3VCDPECFZ4A2MVXNKU2OYIA
------------------------------------
Proxy:
echo "ibfdhqzwbruljahcn4xe5lxhvcq27zxlb3aa7rqhl5enzt4cqnmpfpqd:descriptor:x25519:AASXMXOWENXEL4YF2LSUVA34NBKRMICY35QULAMV4OQTVWJ2BVJA" >/hiddenservice/onion_auth/wallet.auth_private
echo "descriptor:x25519:BSNUTWYLTBA3KVEBK75GNOVNHVXPDJMSN5W7236TDBQJX56J4BZQ" >/hiddenservice/authorized_clients/wallet.auth

Wallet:
echo "l52talc2uiw54cj7wp662dd63gcubqrqfia2uuz3oqzlj2jt2abqhkad:descriptor:x25519:XCAEOF7DWKJUBW7ZBGCKG67X6ZCF4SJBGJC32OWOEWXPAZDGGVDQ" >/hiddenservice/onion_auth/proxy.auth_private
echo "descriptor:x25519:O7NCVDCIYWDHAKQA7QYWYULJWU7GH3VCDPECFZ4A2MVXNKU2OYIA" >/hiddenservice/authorized_clients/proxy.auth
```
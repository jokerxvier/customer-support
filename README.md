# Setup


## Set Environment Variables to root /

Copy env.example to .env. Run command below

```bash
cp .env.example .env # required
```

## Set Environment Variables to **/backend**

```bash
cp .env.example .env # required
```

## Set Environment Variables to **/frontend**

```bash
cp .env.example .env # required
```


## Define Environment Variable in root **/.env**

```bash
API_PORT=4000 # define api port should run
API_URL=http://localhost # define api url or domain
```
### Define Local Database in root **/.env**
```bash
DB_HOST=YOUR_DATABASE_HOST
DB_PORT=YOUR_DATABASE_PORT
DB_DATABASE=YOUR_DATABASE_NAME
DB_USERNAME=YOUR_DATABASE_USERNAME
DB_PASSWORD=YOUR_DATABASE_PASSWORD # password must be strong
DB_SYNCHRONIZE=false # should always be false in production
```
### Define POKERDB CREDENTIALS in root **/.env**
```bash
DB_POKER_CONNECTION_NAME=POKER_DB # name of the connection this is required
DB_POKER_HOST=YOUR_DATABASE_HOST
DB_POKER_PORT=YOUR_DATABASE_PORT
DB_POKER_DATABASE=YOUR_DATABASE_NAME
DB_POKER_USERNAME=YOUR_DATABASE_USERNAME
DB_POKER_PASSWORD=sYOUR_DATABASE_PASSWORD 
DB_POKER_SYNCHRONIZE=false # should always be false in production
```
### Define POKERWEBADMIN CREDENTIALS in root **/.env**
```bash
DB_WEBADMIN_CONNECTION_NAME=POKER_WEBADMIN # name of the connection this is required
DB_POKER_HOST=YOUR_DATABASE_HOST
DB_POKER_PORT=YOUR_DATABASE_PORT
DB_POKER_DATABASE=YOUR_DATABASE_NAME
DB_POKER_USERNAME=YOUR_DATABASE_USERNAME
DB_POKER_PASSWORD=sYOUR_DATABASE_PASSWORD 
DB_POKER_SYNCHRONIZE=false # should always be false in production
```

### Define JWT Config in root **/.env**
```bash
JWT_SECRET=hard!to-guess_secret  # your jwt secret key must be strong type
JWT_EXPIRE=1d
JWT_REFRESH_EXPIRE=14d
```

### NEXTJS ENV
```bash
REACT_APP_PORT=3000 # where front end port will run
```


### RUN DOCKER

`docker-compose up --build`






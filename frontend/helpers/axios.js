import axios from 'axios';
import { authenticationService } from '../services';

axios.defaults.baseURL = process.env.API_URL;
if (authenticationService.getToken) {
    axios.defaults.headers.common = {'Authorization': `Bearer ${authenticationService.getToken.accessToken}`}
}

export default axios;
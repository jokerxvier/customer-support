import { authenticationService } from '../services';

export default function withAuth(Component) {
	return (props) => {
		React.useEffect(() => {
			if (!authenticationService.getToken) {
				//window.location.href = "/login";
				console.log('asd');

				return;
			}
		}, []);

		return (
			<Component {...props} />
		);
	}
}

import { authenticationService } from '../services';
import Router from 'next/router';

export function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if ([401, 403].indexOf(response.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                authenticationService.logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

export function handleAuthResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}


export function handleUnAuthorized(error, ctx) {

    if (error.response) {
        if (error.response.status === 401 && !ctx.req) {
            Router.replace('/login');
            return {};
        } else if (error.response.status === 401 && ctx.req) {
            ctx.res?.writeHead(302, {
                Location: `/login`
            });
            ctx.res?.end();
            return;
        }
    } else if (error.request) {
        if (ctx.req) {
            ctx.res?.writeHead(302, {
                Location: `/login`
            });
            ctx.res?.end();
            return;
        }

        if (!ctx.req) {
            Router.replace('/login');
            return {};
        }
    }
}
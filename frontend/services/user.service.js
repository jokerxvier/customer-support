import axios from '../helpers/axios';
import { authenticationService } from '../services';

export const userService = {
	getAllPlayerInformation
};

function getAllPlayerInformation(username) {
	const token = authenticationService.getToken;
	return axios.all([
		axios.get(`${process.env.API_URL}/users/${username}`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/deposits`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/joined-tournaments`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/latestP2Ptransfer`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/rake/daily`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/rake/lifetime`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/withdrawals?status=completed`, { headers: { 'Authorization': `Bearer ${token}` } }),
		axios.get(`${process.env.API_URL}/users/${username}/withdrawals?status=pending`, { headers: { 'Authorization': `Bearer ${token}` } }),
	]).then(axios.spread((player, playerDeposits, joinedTournaments, latestP2Ptransfer, dailyRakes, lifeTimeRake, completedWithdrawals, pendingWithdrawals) => {
		return {
			player: (player.status === 200) ? player.data : {},
			playerDeposits: (playerDeposits.status === 200) ? playerDeposits.data : [],
			joinedTournaments: (joinedTournaments.status === 200) ? joinedTournaments.data : [],
			latestP2Ptransfer: (latestP2Ptransfer.status === 200) ? latestP2Ptransfer.data : [],
			dailyRakes: (dailyRakes.status === 200) ? dailyRakes.data : [],
			lifeTimeRake: (lifeTimeRake.status === 200) ? lifeTimeRake.data : [],
			completedWithdrawals: (completedWithdrawals.status === 200) ? completedWithdrawals.data : [],
			pendingWithdrawals: (pendingWithdrawals.status === 200) ? pendingWithdrawals.data : [],
		}
	}));
}





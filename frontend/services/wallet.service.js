import axios from '../helpers/axios';
import { authenticationService } from '../services';

export const walletService = {
    validateWalletAddress
};

function validateWalletAddress(type, address) {
    const token = authenticationService.getToken;
    return axios.get(`${process.env.API_URL}/tor/validate/${type}/${address}`, { headers: { 'Authorization': `Bearer ${token}` } });
}
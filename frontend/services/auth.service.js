import { BehaviorSubject } from 'rxjs';
import { Cookies } from 'react-cookie';
import fetch from 'isomorphic-unfetch';
import { handleResponse, handleAuthResponse } from '../helpers/handle-response';

const cookies = new Cookies();
const swcTokenSubject = new BehaviorSubject(cookies.get('SWC_Token'));

export const authenticationService = {
    login,
    logout,
    swcToken: swcTokenSubject.asObservable(),
    get getToken() { return swcTokenSubject.value }
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`${process.env.API_URL}/auth/signin`, requestOptions)
        .then(handleAuthResponse)
        .then(tokens => {
            cookies.set('SWC_Token', tokens.accessToken);
            swcTokenSubject.next(tokens.accessToken);
            return tokens;
        }).catch(error => Promise.reject(error));
}

function logout() {
    cookies.remove('SWC_Token');
    swcTokenSubject.next(null);
}
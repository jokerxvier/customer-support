import Link from 'next/link';
import { withRouter } from 'next/router';

function AdminContent(props) {
    const { pathname } = props.router;
    return (
        <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a className="sidebar-brand d-flex align-items-center justify-content-center" href="#">
                <div className="sidebar-brand-icon rotate-n-15">
                    <i className="fas fa-laugh-wink"></i>
                </div>
                <div className="sidebar-brand-text mx-3">SWC Portal</div>
            </a>

            <hr className="sidebar-divider my-0" />

            <li className={['nav-item', pathname === '/' ? 'active' : ''].join(' ')}>

                <Link href="/">
                    <a className="nav-link">
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </Link>

            </li>

            <hr className="sidebar-divider" />
            <div className="sidebar-heading">Players</div>
            <li className={['nav-item', pathname === '/players' ? 'active' : ''].join(' ')}>
                <Link href="/players">
                    <a className="nav-link">
                        <i className="fas fa-fw fa-search"></i>
                        <span>Search Player</span>
                    </a>
                </Link>
            </li>
            <hr className="sidebar-divider" />
            <div className="sidebar-heading">Wallet</div>
            <li className={['nav-item', pathname === '/wallet' ? 'active' : ''].join(' ')}>

                <Link href="/wallet">
                    <a className="nav-link">
                        <i className="fas fa-fw fa-wallet"></i>
                        <span>Search Address</span>
                    </a>
                </Link>

            </li>
        </ul>
    )
}


export default withRouter(AdminContent);
import React from 'react';
import { Code } from 'react-content-loader'
import _ from 'lodash';
import PlayerTransactionList from './tables/PlayerTransactionList';
import NoAvailableContent from './NoAvailableContent';

const PlayerTaransactions = React.memo(({ transactions, loading, status, title }) => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">{title}</h6>
            </div>
            <div className="card-body">
                {loading && <Code />}
                {!loading && _.isEmpty(transactions) && <NoAvailableContent />}
                {!loading && !_.isEmpty(transactions) &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">Transaction Id</th>
                                <th scope="col">Username</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Currency</th>
                                <th scope="col">Created At</th>
                                {(status === 'completed') && <th scope="col">Finished At</th>}
                                {(status === 'completed') && <th scope="col">Status</th>}
                            </tr>
                        </thead>
                        <tbody>
                            {transactions.length > 0 && transactions.map((item, index) => <PlayerTransactionList item={item} status={status} key={index} />)}
                        </tbody>
                    </table>
                }
            </div>
        </div>
    );
})

export default PlayerTaransactions;
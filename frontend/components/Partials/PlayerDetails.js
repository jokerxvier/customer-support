import React from 'react';
import { BulletList  } from 'react-content-loader'
import _ from 'lodash';
import NoAvailableContent from './NoAvailableContent';

const PlayerDetails = React.memo(({playerDetail, loading, playerRake}) => {
    return (
        
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">Player Details</h6>
            </div>
            <div className="card-body">
                
                {loading && <BulletList  /> }

                {!loading && _.isEmpty(playerDetail) &&  <NoAvailableContent />  }
                   
                {!loading &&  !_.isEmpty(playerDetail) &&
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item text-gray-900"><span className="font-weight-bold text-primary">Username</span> : {playerDetail.username}</li>
                        <li className="list-group-item text-gray-900"><span className="font-weight-bold text-primary">Email</span>: {playerDetail.email}</li>
                        <li className="list-group-item text-gray-900">
                            <span className="font-weight-bold text-primary">isAffiliate</span>: {playerDetail.isAffiliate ? 'Yes' : 'No' } 
                        </li>
                        <li className="list-group-item text-gray-900"><span className="font-weight-bold text-primary">Bonus Code</span>:  &nbsp;
                            {playerDetail.bonusCode && <button type="button" className="btn btn-outline-primary btn-sm"> {playerDetail.bonusCode} </button> } 
                        </li>
                        <li className="list-group-item text-gray-900"><span className="font-weight-bold text-primary">Lifetime Rake</span>:  &nbsp;
                            { !_.isEmpty(playerRake) && playerRake.rake}
                        </li>
                    </ul>
                }
            </div>
         </div>
    );
});

export default PlayerDetails;
import React from 'react';
import { Code  } from 'react-content-loader'
import _ from 'lodash';
import NoAvailableContent from './NoAvailableContent';
import PlayerRakeList from './tables/PlayerRakeList';

const Rake = React.memo(({rakes, loading}) => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">Daily Rake</h6>
            </div>
            <div className="card-body">
                {loading && <Code /> }
                {!loading && _.isEmpty(rakes) &&  <NoAvailableContent />  }
                {!loading && rakes.length > 0 &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">Rake</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            { rakes.length > 0 &&  rakes.map((item, index) => <PlayerRakeList item={item} key={index}/>)}
                        </tbody>
                    </table>
                }
          
            </div>
        </div>
    );
});

export default Rake;
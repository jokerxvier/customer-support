import React from 'react';
import { Code } from 'react-content-loader'
import _ from 'lodash';
import NoAvailableContent from './NoAvailableContent';
import P2pTransferList from './tables/P2pTransferList';

const P2PTransactions = React.memo(({ p2pTransfer, loading, title }) => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">{title}</h6>
            </div>
            <div className="card-body">
                {loading && <Code />}
                {!loading && _.isEmpty(p2pTransfer) && <NoAvailableContent />}
                {!loading && !_.isEmpty(p2pTransfer) &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">SenderTransaction ID</th>
                                <th scope="col">Sender</th>
                                <th scope="col">ReceiverTransaction ID</th>
                                <th scope="col">Receiver</th>
                                <th scope="col">Amount</th>
                                <th>Currency</th>
                                <th scope="col">Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            {p2pTransfer.length > 0 && p2pTransfer.map((item, index) => <P2pTransferList item={item} key={index} />)}
                        </tbody>
                    </table>
                }
            </div>
        </div>
    );
});

export default P2PTransactions;
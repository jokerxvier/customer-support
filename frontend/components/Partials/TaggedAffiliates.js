import React from 'react';
import { Code } from 'react-content-loader';
import _ from 'lodash';
import TaggedAffiliateList from './tables/TaggedAffiliateList';
import NoAvailableContent from './NoAvailableContent';

const TaggedAffiliates = React.memo(({ affiliates, loading, title }) => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">{title}</h6>
            </div>
            <div className="card-body">
                {loading && <Code />}
                {!loading && _.isEmpty(affiliates) && <NoAvailableContent />}
                {!loading && !_.isEmpty(affiliates) > 0 &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">Username</th>
                                <th scope="col">Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            {affiliates.length > 0 && affiliates.map((item, index) => <TaggedAffiliateList item={item} key={index} />)}
                        </tbody>
                    </table>
                }
            </div>
        </div>
    );
});

export default TaggedAffiliates;
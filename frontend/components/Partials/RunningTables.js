import React from 'react';
import { Code } from 'react-content-loader';
import _ from 'lodash';
import RunningTableList from './tables/RunningTableList';
import NoAvailableContent from './NoAvailableContent';

const RunningTables = React.memo(({ runningTables, loading, title }) => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">{title}</h6>
            </div>
            <div className="card-body">
                {loading && <Code />}
                {!loading && _.isEmpty(runningTables) && <NoAvailableContent />}
                {!loading && !_.isEmpty(runningTables) &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Server Id</th>
                                <th scope="col">Start At</th>
                            </tr>
                        </thead>
                        <tbody>
                            {runningTables.length > 0 && runningTables.map((item, index) => <RunningTableList item={item} key={index} />)}
                        </tbody>
                    </table>
                }
            </div>
        </div>
    );
});

export default RunningTables;
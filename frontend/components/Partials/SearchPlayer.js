import { useRef, useCallback } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { userService } from '../../services/user.service';

function SearchUser({ searchResult, isLoading }) {

    const searchInputRef = useRef();

    const handleSubmit = useCallback(e => {
        e.preventDefault();

        const username = searchInputRef.current.value;

        if (!username) {
            return;
        }

        isLoading(true);

        userService.getAllPlayerInformation(username).then((response) => {
            searchResult(response);
            isLoading(false);
        }).catch(error => {
            if (error.response.status === 404) {
                searchResult({
                    player: {},
                    playerDeposits: [],
                    playerWithdraw: [],
                    joinedTournaments: [],
                    latestP2Ptransfer: [],
                    dailyRakes: [],
                    lifeTimeRake: [],
                    completedWithdrawals: [],
                    pendingWithdrawals: []
                });
                toast.error("Player not found!");
            }

            isLoading(false);
        });
    }, [searchInputRef]);

    return (
        <div className="card border-left-primary shadow h-100 py-2">
            <form className="d-sm-inline-block  mr-3 ml-md-3 my-2 my-md-0 mw-100 navbar-search" onSubmit={handleSubmit}>
                <div className="input-group">
                    <input type="text" ref={searchInputRef} className="form-control bg-light border-0 small" placeholder="Search username" aria-label="Search" aria-describedby="basic-addon2" />
                    <div className="input-group-append">
                        <button type="submit" className="btn btn-primary">
                            <i className="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
            <ToastContainer />

        </div>

    );
}

export default SearchUser;

import React from 'react';
import * as moment from 'moment'

function PlayerRakeList({item}) {
    return (
        <tr>
            <td>{item.rake}</td>
            <td>{moment(item.date).format('MMMM Do YYYY, h:mm a')}</td>
        </tr>            
    );
}

export default PlayerRakeList;
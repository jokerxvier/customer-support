import React from 'react';
import * as moment from 'moment'

function PlayerTransactionList({ item, status }) {

    let btnStatus = 'btn-success';

    if (item.status === 'declined') {
        btnStatus = 'btn-danger';
    }

    return (
        <tr key={item.transactionId}>
            <td style={{ wordBreak: "break-all", width: '35%' }}>
                {item.transactionId}
            </td>
            <td>{item.username}</td>
            <td>{item.amount}</td>
            <td>{item.moneyType}</td>
            <td> {(item.createdAt) ? moment(item.createdAt).format('MMMM Do YYYY, h:mm a') : ''}</td>
            {status === 'completed' && <td>{(item.finishAt) && moment(item.finishAt).format('MMMM Do YYYY, h:mm a')}</td>}

            {status === 'completed' &&
                < td >
                    <button type="button" className={`btn ${btnStatus}  btn-sm`}><span className="font-weight-bold">{item.status}</span></button>
                </td>
            }

        </tr >
    );
}

export default PlayerTransactionList;
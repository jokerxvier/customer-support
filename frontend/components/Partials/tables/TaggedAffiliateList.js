import React from 'react';
import * as moment from 'moment';


function TaggedAffiliateList({ item }) {
    return (
        <tr key={item.username}>
            <td>{item.username}</td>
            <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm a')}</td>
        </tr>
    );
}

export default TaggedAffiliateList;
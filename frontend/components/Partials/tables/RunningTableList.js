import React from 'react';
import * as moment from 'moment';


function RunningTableList({ item }) {
    return (
        <tr key={item.id}>
            <td>
                {item.id}
            </td>
            <td>{item.name}</td>
            <td>{item.serverId}</td>
            <td>{moment(item.startDate).format('MMMM Do YYYY, h:mm a')}</td>
        </tr>
    );
}

export default RunningTableList;
import React from 'react';
import * as moment from 'moment'

function TournamentList({item, itemKey}) {
    return (
        <tr key={itemKey}>
            <td>{item.tournamentId}</td>
            <td>{item.name}</td>
            <td>{item.prize}</td>
            <td>{item.buyIn}</td>
            <td>
                {moment(item.joinedAt).format('MMMM Do YYYY, h:mm a')}
            </td>
        </tr>
    );
}

export default TournamentList;
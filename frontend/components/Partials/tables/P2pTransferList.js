import React from 'react';
import * as moment from 'moment'

function P2pTransferList({ item }) {
    return (
        <tr>
            <td>{item.fromTransactionId}</td>
            <td>{item.fromUser}</td>
            <td>{item.toTransactionId}</td>
            <td>{item.toUser}</td>
            <td>{item.amount}</td>
            <td>{item.moneyType}</td>
            <td>{moment(item.createdAt).format('MMMM Do YYYY, h:mm a')}</td>
        </tr>

    );
}

export default P2pTransferList;
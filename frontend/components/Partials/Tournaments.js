import React from 'react';
import { Code } from 'react-content-loader'
import _ from 'lodash';
import TournamentList from './tables/TournamentList';
import NoAvailableContent from './NoAvailableContent';

const Tournaments = React.memo(({ tournaments, loading, title }) => {
    return (
        <div className="card shadow mb-4">
            <div className="card-header py-3">
                <h6 className="m-0 font-weight-bold text-primary">{title}</h6>
            </div>
            <div className="card-body">
                {loading && <Code />}
                {!loading && _.isEmpty(tournaments) && <NoAvailableContent />}
                {!loading && !_.isEmpty(tournaments) &&
                    <table className="table table-bordered dataTable" id="dataTable">
                        <thead>
                            <tr>
                                <th scope="col">Transaction ID</th>
                                <th scope="col">Tournament Name</th>
                                <th scope="col">Prize</th>
                                <th scope="col">Buy In</th>
                                <th scope="col">Joined At</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tournaments.map((item, key) => <TournamentList item={item} itemKey={key} key={key} />)}
                        </tbody>
                    </table>
                }

            </div>
        </div>
    );
});

export default Tournaments;
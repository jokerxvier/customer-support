import { useState, useCallback } from 'react';
import cookies from 'next-cookies';

import AdminLayoutHoc from '../components/Layouts/AdminLayoutHoc';
import SearchUser from '../components/Partials/SearchPlayer';
import PlayerDetails from '../components/Partials/PlayerDetails';
import PlayerTaransactions from '../components/Partials/PlayerTaransactions';
import Tournaments from '../components/Partials/Tournaments';
import P2PTransactions from '../components/Partials/P2PTransactions';
import Rake from '../components/Partials/Rake';
import { handleUnAuthorized } from '../helpers/handle-response';

export default function Player() {
  const [playerDetail, setPlayerDetail] = useState({});
  const [playerLifetimeRake, setPlayerLifetimeTake] = useState({});
  const [playerDailyRake, setPlayerDailyRake] = useState([]);
  const [playerDeposits, setPlayerDeposits] = useState([]);
  const [playerCompletedWithdraw, setPlayerCompletedWithdraw] = useState([]);
  const [playerPendingWithdraw, setPlayerPendingWithdraw] = useState([]);
  const [joinedTournamentList, setJoinedTournamentList] = useState([]);
  const [p2pTransfer, setP2pTransfer] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const searchResult = useCallback((
    {
      player,
      playerDeposits,
      joinedTournaments,
      latestP2Ptransfer,
      dailyRakes,
      lifeTimeRake,
      completedWithdrawals,
      pendingWithdrawals
    }
  ) => {
    setPlayerDetail(player);
    setPlayerDeposits(playerDeposits);
    setJoinedTournamentList(joinedTournaments);
    setP2pTransfer(latestP2Ptransfer);
    setPlayerDailyRake(dailyRakes);
    setPlayerLifetimeTake(lifeTimeRake);
    setPlayerCompletedWithdraw(completedWithdrawals);
    setPlayerPendingWithdraw(pendingWithdrawals);

  }, [
    setPlayerDetail,
    setPlayerDeposits,
    setJoinedTournamentList,
    setP2pTransfer,
    setPlayerLifetimeTake,
    setPlayerDailyRake,
    setPlayerCompletedWithdraw,
    setPlayerPendingWithdraw
  ]);

  const onLoading = useCallback((loading) => {
    setIsLoading(loading);
  }, [isLoading, setIsLoading]);

  return (
    <AdminLayoutHoc>
      <h1 className="h3 mb-4 text-gray-800">Search Player</h1>
      <div className="row  mb-4">
        <div className="col-md-4 ml-auto">
          <SearchUser searchResult={searchResult} isLoading={onLoading} />
        </div>
      </div>

      <div className="row">
        <div className="col-lg-6">
          <PlayerDetails playerDetail={playerDetail} loading={isLoading} playerRake={playerLifetimeRake} />
        </div>

        <div className="col-lg-6">
          <Rake rakes={playerDailyRake} loading={isLoading} />
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <PlayerTaransactions transactions={playerPendingWithdraw} loading={isLoading} status='pending' title="Last 10 Pending Withdrawals" />
        </div>

        <div className="col-lg-12">
          <PlayerTaransactions transactions={playerCompletedWithdraw} loading={isLoading} status='completed' title="Last 10 Completed Withdrawals" />
        </div>

        <div className="col-lg-12">
          <PlayerTaransactions transactions={playerDeposits} loading={isLoading} title="Last 10 Deposits" />
        </div>

        <div className="col-lg-12">
          <Tournaments tournaments={joinedTournamentList} loading={isLoading} title="Last 10 Joined Tournaments" />
        </div>

        <div className="col-lg-12">
          <P2PTransactions p2pTransfer={p2pTransfer} loading={isLoading} title="Last 10 P2P Transactions" />
        </div>
      </div>
    </AdminLayoutHoc>
  );
}

Player.getInitialProps = async (ctx) => {
  if (!cookies(ctx).SWC_Token) {
    const error = {
      response: {
        status: 401
      }
    };

    handleUnAuthorized(error, ctx);
    return;
  }

  return {};
}
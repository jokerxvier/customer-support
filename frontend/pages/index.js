import { useState } from 'react';
import axios from 'axios';
import cookies from 'next-cookies';

import AdminLayoutHoc from '../components/Layouts/AdminLayoutHoc';
import PlayerTaransactions from '../components/Partials/PlayerTaransactions';
import RunningTables from '../components/Partials/RunningTables';
import TaggedAffiliates from '../components/Partials/TaggedAffiliates';

import { handleUnAuthorized } from '../helpers/handle-response';


export default function Home({ latestDeposits, latestPendingWithdrawals, latestCompletedWithdrawals, latestRunningTables, taggedAffliates }) {

  const [isLoading, setIsLoading] = useState(false);

  return (
    <AdminLayoutHoc>
      <h1 className="h3 mb-4 text-gray-800">Dashboard</h1>
      <div className="row">
        <div className="col-lg-6">
          <div className="col">
            <PlayerTaransactions transactions={latestPendingWithdrawals} loading={isLoading} status='pending' title="Latest Pending 10 Withdrawal" />
          </div>

          <div className="col">
            <PlayerTaransactions transactions={latestCompletedWithdrawals} loading={isLoading} status='completed' title="Latest Completed 10 Withdrawal" />
          </div>

          <div className="col">
            <PlayerTaransactions transactions={latestDeposits} loading={isLoading} title="Latest 10 Deposits" />
          </div>

        </div>

        <div className="col-lg-6">
          <div className="col">
            <RunningTables runningTables={latestRunningTables} loading={isLoading} title="Latest 10 Running Tables" />
          </div>

          <div className="col">
            <TaggedAffiliates affiliates={taggedAffliates} loading={isLoading} title="Latest players tag by affiliate" />
          </div>
        </div>

      </div>
    </AdminLayoutHoc>
  );
}

Home.getInitialProps = async (ctx) => {

  const token = cookies(ctx).SWC_Token || '';

  try {
    const [latestDeposits, latestPendingWithdrawals, latestCompletedWithdrawals, latestRunningTables, taggedAffliates] = await axios.all([
      axios.get(`${process.env.API_URL}/transaction/latest-deposits`, { headers: { 'Authorization': `Bearer ${token}` } }),
      axios.get(`${process.env.API_URL}/transaction/latest-withdrawals?status=pending`, { headers: { 'Authorization': `Bearer ${token}` } }),
      axios.get(`${process.env.API_URL}/transaction/latest-withdrawals?status=completed`, { headers: { 'Authorization': `Bearer ${token}` } }),
      axios.get(`${process.env.API_URL}/table/last-running-tables`, { headers: { 'Authorization': `Bearer ${token}` } }),
      axios.get(`${process.env.API_URL}/users/latest-tagged-by-affiliate`, { headers: { 'Authorization': `Bearer ${token}` } }),
    ]);

    return {
      latestDeposits: latestDeposits.data,
      latestPendingWithdrawals: latestPendingWithdrawals.data,
      latestCompletedWithdrawals: latestCompletedWithdrawals.data,
      latestRunningTables: latestRunningTables.data,
      taggedAffliates: taggedAffliates.data
    };
  } catch (error) {
    handleUnAuthorized(error, ctx);
    return {};
  }
}



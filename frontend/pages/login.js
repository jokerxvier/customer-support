import Router from 'next/router';
import { Formik } from 'formik';
import GuessLayoutHoc from '../components/Layouts/GuessLayoutHoc';
import { authenticationService } from '../services';

function Login() {
    return (
        <GuessLayoutHoc>
            <div className="card o-hidden border-0 shadow-lg my-5 mt-5">
                <div className="card-body p-0">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="p-5">
                                <div className="text-center mb-3">
                                    <img src="/static/assets/images/logo.svg" alt="logo" width="100" />
                                </div>
                                <Formik
                                    initialValues={{ username: '', password: '' }}
                                    validate={values => {
                                        const errors = {};
                                        if (!values.password) {
                                            errors.password = 'Required';
                                        }

                                        if (!values.username) {
                                            errors.username = 'Required';
                                        }

                                        return errors;
                                    }}
                                    onSubmit={async (values, { setStatus, setSubmitting }) => {
                                        setStatus();

                                        authenticationService.login(values.username, values.password).then(authToken => {
                                            Router.push('/');
                                            return;
                                        }).catch(error => {
                                            setStatus('Invalid Credentials');
                                            setSubmitting(false);
                                        });
                                    }}
                                >
                                    {({
                                        values,
                                        status,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                    }) => (

                                            <form className="user needs-validation" onSubmit={handleSubmit}>
                                                {status && <div className={'alert alert-danger'}>{status}</div>}
                                                <div className="form-group has-error has-feedback">
                                                    <input
                                                        type="username"
                                                        name="username"
                                                        className={`form-control form-control-user ${errors.username && touched.username && 'is-invalid'}`}
                                                        id="exampleInputEmail"
                                                        placeholder="Username"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.username}
                                                    />
                                                    {errors.username && touched.username && <div className="invalid-feedback pl-2"> {errors.username}</div>}
                                                </div>

                                                <div className="form-group">
                                                    <input
                                                        type="password"
                                                        name="password"
                                                        className={`form-control form-control-user ${errors.password && touched.password && 'is-invalid'}`}
                                                        id="exampleInputPassword"
                                                        placeholder="Password"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.password}
                                                    />
                                                    {errors.password && touched.password && <div className="invalid-feedback pl-2"> {errors.password}</div>}
                                                </div>
                                                <button type="submit" disabled={isSubmitting} className="btn btn-primary btn-user btn-block">
                                                    Login
                                            </button>

                                            </form>
                                        )}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </GuessLayoutHoc>
    );
}

export default Login;
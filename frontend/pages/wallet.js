import { useRef, useCallback, useState } from 'react';
import cookies from 'next-cookies';
import { ToastContainer, toast } from 'react-toastify';
import { handleUnAuthorized } from '../helpers/handle-response';
import AdminLayoutHoc from '../components/Layouts/AdminLayoutHoc';
import { walletService } from '../services/wallet.service';

export default function Wallet() {

    const [validAddress, setValidAddress] = useState(false);
    const [validationColor, setValidationColor] = useState('');
    const [validationMessage, setValidationMessage] = useState('');
    const [isSubmitted, setIsSubmitted] = useState(false);

    const selectInputRef = useRef();
    const walletInputRef = useRef();

    const handleSubmit = useCallback(e => {
        e.preventDefault();

        const walletAddresss = walletInputRef.current.value;
        const walletType = selectInputRef.current.value;

        if (!walletAddresss || !walletType) {

            toast.error("Wallet Type and Address is required");
            return;
        }

        walletService.validateWalletAddress(walletType, walletAddresss).then(({ data }) => {
            const { isValid, isMine } = data;

            if (!isValid && !isMine) {
                setValidationMessage('Address is invalid');
                setValidationColor('danger');
            }
            
            if (isValid && !isMine) {
                setValidationMessage("Address is valid but doesn't belong to our wallet");
                setValidationColor('warning');
            }

            if (isValid && isMine) {
                setValidationMessage("Address is valid and belongs to our wallet.");
                setValidationColor('success');
            }
            
            setValidAddress(isValid);
            setIsSubmitted(true);
        }).catch(error => {
            setValidationColor('');
            setValidationMessage('');
            setValidAddress(false);
            setIsSubmitted(true);
        });

    }, [walletInputRef, selectInputRef, setValidAddress, setIsSubmitted]);

    return (
        <AdminLayoutHoc>
            <h1 className="h3 mb-4 text-gray-800">Validate Wallet Address</h1>

            <div className="row">
                <div className="col-lg-4">
                    {validationColor &&  validationMessage && isSubmitted &&
                        <div className={`alert alert-${validationColor}`} role="alert">
                            {validationMessage}
                        </div>
                    }
                    <div className="card shadow mb-4">
                        <div className="card-body">
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label>Choose Wallet Type</label>
                                    <select ref={selectInputRef} className="form-control" id="exampleFormControlSelect1">
                                        <option>BTC</option>
                                        <option>BCH</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <label>Wallet Address</label>
                                    <input type="text" ref={walletInputRef} className="form-control" id="exampleFormControlInput1" placeholder="BTC/BCH Address" />
                                </div>
                                <button type="submit" className="btn btn-primary mb-2">Validate</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer />
        </AdminLayoutHoc>
    )
}

Wallet.getInitialProps = async (ctx) => {
    if (!cookies(ctx).SWC_Token) {
        const error = {
            response: {
                status: 401
            }
        };

        handleUnAuthorized(error, ctx);
        return;
    }

    return {};
}
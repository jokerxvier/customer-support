import { Controller, UseGuards, UseInterceptors, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TransformInterceptor } from 'src/shared/interceptors/transform.interceptor';
import { ConfigService } from '@nestjs/config';
import { TableService } from './table.service';

@Controller('table')
@UseGuards(AuthGuard())
@UseInterceptors(TransformInterceptor)
export class TableController {
  constructor(private configService: ConfigService, private table: TableService) {

  }

  @Get('/last-running-tables')
  getLastRunningTable() {
    return this.table.getLastRunningTables();
  }
}

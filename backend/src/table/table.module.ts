import { Module } from '@nestjs/common';
import 'dotenv/config';
import { TableController } from './table.controller';
import { TableService } from './table.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TableRepository } from 'src/shared/repositories/table.repository';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TableRepository,
    ], process.env.DB_POKER_CONNECTION_NAME),
    AuthModule
  ],
  controllers: [TableController],
  providers: [TableService, TableRepository]
})
export class TableModule { }

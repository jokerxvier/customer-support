import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TableRepository } from 'src/shared/repositories/table.repository';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TableService {
  constructor(private configService: ConfigService,
    @InjectRepository(TableRepository, process.env.DB_POKER_CONNECTION_NAME)
    private tableRepository: TableRepository,
  ) { }

  async getLastRunningTables() {
    const runningTables = await this.tableRepository.getLastRunningTables();
    const result = runningTables.map(async item => {
      return {
        id: item.id,
        name: item.fName,
        startDate: item.fStartedStamp,
        serverId: item.fServerId
      }
    });

    return await this.toResponseObject(result);
  }

  private async toResponseObject(result): Promise<any> {
    return await Promise.all(result).then(values => values.filter(v => v));
  }
}

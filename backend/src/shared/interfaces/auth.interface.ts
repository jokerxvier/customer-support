export interface IAuthResponseData {
  accessToken: string;
  refreshToken: string;
  expiresIn: string;
}
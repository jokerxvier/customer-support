export interface ITournamentResponseData {
  tournamentId: number;
  prize: number;
  name: string;
  buyIn: number;
  joinedAt: Date;
}
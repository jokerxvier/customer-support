export interface ITransactionResponseData {
  transactionId: string;
  moneyTx: string;
  amount: number;
  createdAt: Date;
  finishAt: Date;
  state: string
}

export interface IP2PTransactionResponeData {
  fromTransactionId: number;
  fromUser: string;
  toTransactionId: number;
  toUser: string,
  amount: number,
  createdAt: Date
}
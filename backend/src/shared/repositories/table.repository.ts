import { Table } from "../entities/pokerdb/table.entity";
import { Repository, EntityRepository } from "typeorm";

@EntityRepository(Table)
export class TableRepository extends Repository<Table> {
  async getLastRunningTables(): Promise<Table[]> {
    const runningTables = await this.createQueryBuilder()
      .andWhere("f_ended_stamp = :fDatetime", { fDatetime: '0000-00-00 00:00:00' })
      .orderBy("f_id", 'DESC')
      .take(10)
      .getMany();

    return runningTables;
  }
}
import { Repository, EntityRepository } from "typeorm";
import { TournamentMember } from "../entities/poker-webadmin/tournament-member.entity";
import { User } from "../entities/pokerdb/user.entity";

@EntityRepository(TournamentMember)
export class TournamentMemberRepository extends Repository<TournamentMember> {
  async getUserLastJoinTournament(user: User): Promise<TournamentMember[]> {
    const tournaments = await this.createQueryBuilder()
    .andWhere("f_player_id = :id", { id: user.id })
    .orderBy("f_stamp", 'DESC')
    .take(10)
    .getMany();

    return tournaments;
  }
}
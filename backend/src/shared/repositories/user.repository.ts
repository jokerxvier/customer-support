import { Repository,  EntityRepository } from "typeorm";
import { User } from "../entities/pokerdb/user.entity";
import { AuthCredentialsDto } from "../../auth/dto/auth-credentials.dto";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async validateUserPassword(authCredentialsDto: AuthCredentialsDto): Promise<any> {
        const {username, password} = authCredentialsDto;
        const user = await this.findOne({username});

        if (user && await user.validatePassword(password)) {
            return user;
        } else {
            return null;
        }
    }
} 
import { MoneyTransaction } from "../entities/pokerdb/money-transaction.entity";
import { Repository, EntityRepository } from "typeorm";

import { AffiliatePlayerLog } from "../entities/poker-webadmin/affiliate-player-log.entity";

@EntityRepository(AffiliatePlayerLog)
export class AffiliatePlayerLogRepository extends Repository<AffiliatePlayerLog> {
  async getLatestPlayerTagByAffiliate(): Promise<AffiliatePlayerLog[]> {
    const results = await this.createQueryBuilder()
      .orderBy("f_created_at", 'DESC')
      .take(10)
      .getMany();

    return results;
  }
}
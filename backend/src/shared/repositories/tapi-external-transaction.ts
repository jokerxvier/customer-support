import { Repository, EntityRepository } from "typeorm";
import { TapiExternalTransaction } from "../entities/poker-webadmin/tapi-external-transaction.entity";

@EntityRepository(TapiExternalTransaction)
export class TapiExternalTransactionRepository extends Repository<TapiExternalTransaction> { }
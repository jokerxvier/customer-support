import { Repository, EntityRepository } from "typeorm";
import { Tournament } from "../entities/pokerdb/tournament.entity";

@EntityRepository(Tournament)
export class TournamentRepository extends Repository<Tournament> {
  
}
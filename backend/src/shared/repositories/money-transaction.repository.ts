import { MoneyTransaction } from "../entities/pokerdb/money-transaction.entity";
import 'dotenv/config';
import { Repository, EntityRepository, Brackets } from "typeorm";
import { User } from "../entities/pokerdb/user.entity";
import { Participation } from "../entities/pokerdb/participation.entity";
import { PaymentTransaction } from "../entities/poker-webadmin/payment-transaction.entity";

@EntityRepository(MoneyTransaction)
export class MoneyTransactionRepository extends Repository<MoneyTransaction> {

  async getLastTransactions(transactionType: number, status: string): Promise<any[]> {
    let transactions = await this.createQueryBuilder('mt')
      .select('p.f_external_id', 'externalTransactionId')
      .addSelect('mt.f_money_type', 'moneyType')
      .addSelect('mt.f_pended', 'txStatus')
      .addSelect('mt.f_id', 'moneyTx')
      .addSelect('p.f_amount', 'amount')
      .addSelect('p.f_player_id', 'playerId')
      .addSelect('p.f_create_time', 'fStamp')
      .addSelect('p.f_finish_time', 'finishTime')
      .addFrom(`${process.env.DB_WEBADMIN_DATABASE}.t_payment_transactions`, "p")
      .andWhere("p.f_internal_id = mt.f_id");

    if (status === 'completed') {
      transactions = transactions.andWhere(new Brackets(qb => {
        qb.where("mt.f_pended = :fCompleted", { fCompleted: 0 })
          .orWhere("mt.f_pended = :fDeclined", { fDeclined: 2 })
      }));
    }

    if (status === 'pending') {
      transactions = transactions.andWhere("mt.f_pended  = :fPended", { fPended: 4 });
    }


    const query = transactions.andWhere("mt.f_type = :fType", { fType: transactionType })
      .orderBy("p.f_create_time", 'DESC')
      .take(10) //get first 30 record only
      .getRawMany();

    return query;
  }

  async getUserLastTransactions(user: User, transactionType: number, status: string): Promise<any[]> {

    let transactions = await this.createQueryBuilder('mt')
      .select('p.f_external_id', 'externalTransactionId')
      .addSelect('mt.f_money_type', 'moneyType')
      .addSelect('mt.f_pended', 'txStatus')
      .addSelect('mt.f_id', 'moneyTx')
      .addSelect('p.f_amount', 'amount')
      .addSelect('p.f_player_id', 'playerId')
      .addSelect('p.f_create_time', 'fStamp')
      .addSelect('p.f_finish_time', 'finishTime')
      .addFrom(`${process.env.DB_WEBADMIN_DATABASE}.t_payment_transactions`, "p")
      .andWhere("p.f_internal_id = mt.f_id");

    if (status === 'completed') {
      transactions = transactions.andWhere(new Brackets(qb => {
        qb.where("mt.f_pended = :fCompleted", { fCompleted: 0 })
          .orWhere("mt.f_pended = :fDeclined", { fDeclined: 2 })
      }));
    }

    if (status === 'pending') {
      transactions = transactions.andWhere("mt.f_pended  = :fPended", { fPended: 4 });
    }


    const query = transactions.andWhere("mt.f_type = :fType", { fType: transactionType })
      .andWhere("p.f_player_id = :fPlayerId", { fPlayerId: user.id })
      .orderBy("p.f_create_time", 'DESC')
      .take(10) //get first 30 record only
      .getRawMany();

    return query;
  }

  async getDeposits(transactionType: number, user?: User) {

    let transactions = await this.createQueryBuilder('mt')
      .select('tapi.f_external_transaction_id', 'externalTransactionId')
      .addSelect('tapi.f_amount', 'amount')
      .addSelect('mt.f_money_type', 'moneyType')
      .addSelect('mt.f_player_id', 'playerId')
      .addSelect('tapi.f_creation_date', 'fStamp')
      .addSelect('tapi.f_state', 'state')
      .addFrom(`${process.env.DB_WEBADMIN_DATABASE}.tapi_external_transaction`, "tapi")
      .andWhere("tapi.f_internal_transaction_id = mt.f_id")
      .andWhere("mt.f_type = :fType", { fType: transactionType })

    if (user) {
      transactions = transactions.andWhere("mt.f_player_id = :fPlayerId", { fPlayerId: user.id })
    }
    const query = transactions.orderBy("tapi.f_creation_date", 'DESC')
      .take(10)
      .getRawMany();

    return query;
  }

  async getSenderLastP2PTransactions(user: User, transactionType: number): Promise<MoneyTransaction[]> {
    const userSender = await this.createQueryBuilder()
      .andWhere("f_player_id = :playerId", { playerId: user.id })
      .andWhere("f_type = :fType", { fType: transactionType })
      .andWhere("f_subtype = :fSubtype", { fSubtype: 0 })
      .getMany();

    return userSender;
  }

  async getReceiverTransaction(transaction: MoneyTransaction, transactionType: number): Promise<MoneyTransaction> {
    const userSender = await this.createQueryBuilder()
      .andWhere("f_param_transaction_id = :paramTransactionId", { paramTransactionId: transaction.id })
      .andWhere("f_type = :fType", { fType: transactionType })
      .andWhere("f_subtype = :fSubtype", { fSubtype: 0 })
      .getOne();

    return userSender;
  }

  async getUserDailyTransactionRake(user: User, transactionType: number): Promise<any[]> {
    const rakes = await this.createQueryBuilder('mt')
      .innerJoin(Participation, 'p', 'mt.f_param_game_id = p.f_game_id')
      .select('CAST(mt.f_stamp as date)', 'date')
      .addSelect('AVG(p.f_rake)', 'rake')
      .andWhere('mt.f_player_id = :playerId', { playerId: user.id })
      .andWhere('mt.f_type = :fType', { fType: transactionType })
      .andWhere('mt.f_subtype = :fSubtype', { fSubtype: 0 })
      .groupBy('mt.f_param_game_id')
      .addGroupBy('CAST(mt.f_stamp as date)')
      .orderBy("date", 'DESC')
      .take(30) //get first 30 record only
      .getRawMany();

    return rakes;
  }

  async getUserLifetimeTransactionRake(user: User, transactionType: number): Promise<any> {
    const rakes = await this.createQueryBuilder('mt')
      .innerJoin(Participation, 'p', 'mt.f_param_game_id = p.f_game_id')
      .select('AVG(p.f_rake)', 'rake')
      .andWhere('mt.f_player_id = :playerId', { playerId: user.id })
      .andWhere('mt.f_type = :fType', { fType: transactionType })
      .andWhere('mt.f_subtype = :fSubtype', { fSubtype: 0 })
      .take(30) //get first 30 record only
      .getRawOne();

    return rakes;
  }

}
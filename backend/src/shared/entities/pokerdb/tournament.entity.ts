import { Column, Entity,  BaseEntity, PrimaryGeneratedColumn } from "typeorm";
import * as moment from  'moment';
import { Transform } from "class-transformer";

@Entity("t_tournament", { schema: "poker_db" })
export class Tournament extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "int", name: "f_id", unsigned: true })
  id: number;

  @Column("varchar", { name: "f_name", length: 255 })
  fName: string;

  @Column("bigint", { name: "f_buy_in", default: () => "'0'" })
  fBuyIn: number;

  @Column("datetime", {
    name: "f_start_date",
    default: () => "'0000-00-00 00:00:00'",
  })
  @Transform(fStamp => (moment(fStamp).isValid()) ? moment(fStamp).format('YYYY-MM-DD HH:mm:ss'): '')
  fStartDate: Date;

  @Column("datetime", {
    name: "f_created_stamp",
    default: () => "'0000-00-00 00:00:00'",
  })
  @Transform(fCreatedStamp => (moment(fCreatedStamp).isValid()) ? moment(fCreatedStamp).format('YYYY-MM-DD HH:mm:ss'): '')
  fCreatedStamp: Date;
}
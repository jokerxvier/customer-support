import { Column, Entity, Index } from "typeorm";
import * as moment from 'moment';
import { Transform } from "class-transformer";
@Entity("t_table", { schema: "poker_db" })

export class Table {
  @Column("int", {
    primary: true,
    name: "f_id",
    unsigned: true,
    default: () => "'0'",
  })
  id: number;

  @Column("datetime", {
    name: "f_started_stamp",
    default: () => "'0000-00-00 00:00:00'",
  })
  @Transform(fStartedStamp => (moment(fStartedStamp).isValid()) ? moment(fStartedStamp).format('YYYY-MM-DD HH:mm:ss') : '')
  fStartedStamp: Date;

  @Column("datetime", {
    name: "f_ended_stamp",
    default: () => "'0000-00-00 00:00:00'",
  })
  @Transform(fEndedStamp => (moment(fEndedStamp).isValid()) ? moment(fEndedStamp).format('YYYY-MM-DD HH:mm:ss') : '')
  fEndedStamp: Date;

  @Column("varchar", { name: "f_name", length: 255 })
  fName: string;

  @Column("int", { name: "f_server_id", unsigned: true, default: () => "'0'" })
  fServerId: number;
}
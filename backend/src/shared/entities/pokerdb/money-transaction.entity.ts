import { Column, Entity, ManyToOne, JoinColumn, BaseEntity, OneToOne, OneToMany } from "typeorm";
import * as moment from 'moment';
import { Transform } from "class-transformer";
import { User } from "./user.entity";
import { Participation } from "./participation.entity";

@Entity("t_money_transactions", { schema: "poker_db" })
export class MoneyTransaction extends BaseEntity {
  @Column("int", { primary: true, name: "f_id", unsigned: true, default: () => "'0'" })
  id: number;

  @Column("int", { name: "f_type", unsigned: true, default: () => "'0'" })
  fType: number;

  @Column("int", { name: "f_subtype", default: () => "'0'" })
  fSubtype: number;

  @Column("bigint", { name: "f_value", default: () => "'0'" })
  fValue: number;

  @Column("int", {
    name: "f_param_transaction_id",
    nullable: true,
    unsigned: true,
  })
  fParamTransactionId: number | null;

  @Column("char", { name: "f_money_type", length: 1 })
  fMoneyType: string;

  @Column("int", { name: "f_player_id", unsigned: true, default: () => "'0'" })
  playerId: number;

  @Column("int", { name: "f_param_game_id", nullable: true, unsigned: true })
  fParamGameId: number | null;

  //JOIN
  @ManyToOne(() => User, user => user.moneyTransactions)
  @JoinColumn({ name: "f_player_id" })
  user: User;

  @Column("datetime", { name: "f_stamp", default: () => "CURRENT_TIMESTAMP" })
  @Transform(fStamp => (moment(fStamp).isValid()) ? moment(fStamp).format('YYYY-MM-DD HH:mm:ss') : '')
  fStamp: Date;

}
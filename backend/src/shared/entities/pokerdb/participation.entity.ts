import { Column, Entity, ManyToOne, JoinColumn, BaseEntity } from "typeorm";
import { MoneyTransaction } from "./money-transaction.entity";

@Entity("t_participation", { schema: "poker_db" })
export class Participation extends BaseEntity {
  @Column("int", { primary: true, name: "f_id", unsigned: true, default: () => "'0'"})
  id: number;

  @Column("bigint", { name: "f_rake", default: () => "'0'" })
  fRake: number;

  @Column("int", { name: "f_game_id", unsigned: true, default: () => "'0'" })
  fGameId: number;

  @Column("int", { name: "f_player_id", unsigned: true, default: () => "'0'" })
  playerId: number;
}
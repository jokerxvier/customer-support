import { BaseEntity, Entity, Column, OneToMany } from "typeorm";
import { Exclude, Expose, Transform } from 'class-transformer';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';
import { MoneyTransaction } from "./money-transaction.entity";

@Entity("t_player", { schema: "poker_db" })
export class User extends BaseEntity {
  @Column("int", { primary: true, name: "f_id", unsigned: true, default: () => "'0'" })
  id: number;

  @Column("varchar", { name: "f_nick", unique: true, length: 32 })
  username: string;

  @Column("varchar", { name: "f_mail", length: 255 })
  email: string;

  @Column("varchar", { name: "f_password", length: 255 })
  @Exclude()
  password: string;

  @Column("tinyint", { name: "f_active", default: () => "'0'" })
  isActive: number;

  @Column("tinyint", { name: "f_verified", default: () => "'0'" })
  isVerified: number;

  @Column("tinyint", { name: "f_affiliate", default: () => "'0'" })
  isAffiliate: number;

  @Column("datetime", {
    name: "f_register_stamp",
    default: () => "'0000-00-00 00:00:00'",
  })
  @Transform(registerAt => (moment(registerAt).isValid()) ? moment(registerAt).format('YYYY-MM-DD HH:mm:ss') : '')
  registerAt: Date;

  @Column("tinyint", { name: "f_blocked", default: () => "'0'" })
  isBlocked: number;

  @Column("datetime", {
    name: "f_blocking_limit",
    default: () => "'0000-00-00 00:00:00'",
  })
  @Transform(blockDate => (moment(blockDate).isValid()) ? moment(blockDate).format('YYYY-MM-DD HH:mm:ss') : '')
  blockDate: Date;

  @Column("varchar", { name: "f_bonus_code", length: 100 })
  bonusCode: string;

  @Column("char", { name: "f_tfa_type", length: 1, default: () => "'_'" })
  tfaType: string;

  @OneToMany(() => MoneyTransaction, transaction => transaction.user)
  moneyTransactions: MoneyTransaction[]

  async validatePassword(password: string): Promise<boolean> {
    const hash = this.password.replace(/^\$2y(.+)$/i, '$2a$1');
    const match = await bcrypt.compare(password, hash);
    return match;
  }
}
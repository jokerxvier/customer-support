import { Column, Entity,  BaseEntity, PrimaryGeneratedColumn } from "typeorm";
import * as moment from  'moment';
import { Transform } from "class-transformer";

@Entity("t_tournament_members", { schema: "poker_webadmin" })
export class TournamentMember extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "int", name: "f_id", unsigned: true })
  id: number;

  @Column("int", { name: "f_tournament_id", unsigned: true })
  fTournamentId: number;

  @Column("int", { name: "f_tournament_type", default: () => "'0'" })
  fTournamentType: number;

  @Column("int", { name: "f_player_id", unsigned: true })
  fPlayerId: number;

  @Column("bigint", { name: "f_buy_in", unsigned: true, default: () => "'0'" })
  fBuyIn: number;

  @Column("bigint", {
    name: "f_entry_fee",
    unsigned: true,
    default: () => "'0'",
  })
  fEntryFee: number;

  @Column("bigint", {
    name: "f_prize",
    nullable: true,
    unsigned: true,
    default: () => "'0'",
  })
  fPrize: number | null;

  @Column("datetime", { name: "f_stamp" })
  @Transform(fStamp => (moment(fStamp).isValid()) ? moment(fStamp).format('YYYY-MM-DD HH:mm:ss'): '')
  fStamp: Date;
}
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, PrimaryColumn } from "typeorm";
import * as moment from 'moment';
import { Transform } from "class-transformer";
import { IsOptional } from "class-validator";

@Entity("t_affiliate_players_log", { schema: "poker_webadmin" })
export class AffiliatePlayerLog extends BaseEntity {

  @PrimaryColumn("int", { name: "f_player_id", unsigned: true })
  fPlayerId: number;

  @Column("int", { name: "f_affiliate_id", unsigned: true })
  fAffiliateId: number;


  @Column("timestamp", {
    name: "f_created_at",
    default: () => "CURRENT_TIMESTAMP",
  })
  fCreatedAt: Date;

  @Column("timestamp", { name: "f_deleted_at", nullable: true })
  fDeletedAt: Date | null;

}
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, PrimaryColumn } from "typeorm";
import 'dotenv/config';

@Entity("t_payment_transactions")
export class PaymentTransaction extends BaseEntity {
    @PrimaryColumn({ type: "int", name: "f_id" })
    fId: number;

    @Column("int", { name: "f_internal_id", nullable: true, unique: true })
    fInternalId: number | null;

    @Column("int", { name: "f_player_id" })
    fPlayerId: number;

    @Column("varchar", { name: "f_external_id", nullable: true, length: 255 })
    fExternalId: string | null;

    @Column("bigint", { name: "f_amount", default: () => "'0'" })
    fAmount: string;
}
import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import * as moment from  'moment';
import { Transform } from "class-transformer";

@Entity("tapi_external_transaction", { schema: "poker_webadmin" })
export class TapiExternalTransaction extends BaseEntity {

    @PrimaryGeneratedColumn({ type: "int", name: "f_id", unsigned: true })
    id: number;

    @Column("varchar", { name: "f_currency", nullable: true, length: 50 })
    currency: string | null;

    @Column("bigint", { name: "f_amount", unsigned: true })
    amount: number;

    @Column("enum", {
        name: "f_state",
        enum: ["init", "created", "pended", "processed"],
        default: () => "'init'",
    })
    state: "init" | "created" | "pended" | "processed";

    @Column("char", { name: "f_money_type", nullable: true, length: 1 })
    moneyType: string | null;

    @Column("datetime", { name: "f_creation_date" })
    @Transform( createdDate => (moment(createdDate).isValid()) ?  moment(createdDate).format('YYYY-MM-DD HH:mm:ss') : '')
    createdDate: Date;

    @Column("int", {
        name: "f_internal_transaction_id",
        nullable: true,
        unsigned: true,
    })
    moneyTransactionId: number | null;

    @Column("varchar", { name: "f_external_transaction_id", length: 255 })
    externalTransactionId: string;
}
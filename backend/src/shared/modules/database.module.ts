import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import 'dotenv/config';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USERNAME'),
        password: configService.get<string>('DB_PASSWORD'),
        database: configService.get<string>('DB_DATABASE'),
        entities: [__dirname + '/../../**/*.entity{.ts,.js}'],
        synchronize: configService.get('DB_SYNCHRONIZE') =="true"
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      name: process.env.DB_POKER_CONNECTION_NAME,
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('DB_POKER_HOST'),
        port: configService.get<number>('DB_POKER_PORT'),
        username: configService.get<string>('DB_POKER_USERNAME'),
        password: configService.get<string>('DB_POKER_PASSWORD'),
        database: configService.get<string>('DB_POKER_DATABASE'),
        entities: [__dirname + '/../../**/pokerdb/*.entity{.ts,.js}'],
        synchronize: configService.get('DB_POKER_SYNCHRONIZE') =="true"
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      name: process.env.DB_WEBADMIN_CONNECTION_NAME,
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('DB_WEBADMIN_HOST'),
        port: configService.get<number>('DB_WEBADMIN_PORT'),
        username: configService.get<string>('DB_WEBADMIN_USERNAME'),
        password: configService.get<string>('DB_WEBADMIN_PASSWORD'),
        database: configService.get<string>('DB_WEBADMIN_DATABASE'),
        entities: [__dirname + '/../../**/poker-webadmin/*.entity{.ts,.js}'],
        synchronize: configService.get('DB_WEBADMIN_SYNCHRONIZE') =="true"
      }),
      inject: [ConfigService],
    }),
  ]

})
export class DatabaseModule {}

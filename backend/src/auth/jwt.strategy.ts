import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtPayload } from './jwt-payload.interface';
import { UserRepository } from '../shared/repositories/user.repository';
import  'dotenv/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(UserRepository, process.env.DB_POKER_CONNECTION_NAME)
        private userRepository: UserRepository,
        private configService: ConfigService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.get<string>('JWT_SECRET')
        }); 
    }

    async validate(payload: JwtPayload): Promise<any> {
        const { username } = payload;
        const user = await this.userRepository.findOne({ username });

        if (!user) {
          throw new UnauthorizedException();
        }

        return user;
    }
}
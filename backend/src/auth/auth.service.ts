import { Injectable, UnauthorizedException, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from '../shared/repositories/user.repository';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtPayload } from './jwt-payload.interface';
import  'dotenv/config';
import { ConfigService } from '@nestjs/config';
import { IAuthResponseData } from 'src/shared/interfaces/auth.interface';


@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository, process.env.DB_POKER_CONNECTION_NAME)
    private userRepository: UserRepository,
    private jwtService: JwtService,
    private configService: ConfigService
  ) {}

  async signIn(authCredentialsDto: AuthCredentialsDto) : Promise<IAuthResponseData> {
    const user =  await this.userRepository.validateUserPassword(authCredentialsDto);

    if (!user) {
      throw new UnauthorizedException('Invalid Credentials');
    }

    const payload : JwtPayload = { username : user.username }
    return this.generateToken(payload);
  }

  async refreshToken(bearer: string): Promise<IAuthResponseData> {
    
    if (!bearer) {
      throw new UnauthorizedException('Invalid Crendentials');
    }
    
    try {
      const token = bearer.split(' ')[1];
      const verify = await this.jwtService.verify(token);
      const payload : JwtPayload = { username : verify.username }
      return this.generateToken(payload);
    }catch(e) {
      throw new UnauthorizedException('Invalid Token');
    }
  }

  private async generateToken(payload: JwtPayload): Promise<IAuthResponseData> {
    const accessToken = await this.jwtService.sign(payload);
    const refreshToken = await this.jwtService.sign(payload, {expiresIn: this.configService.get<string>('JWT_REFRESH_EXPIRE')});
    return this.toResponseObject(accessToken, refreshToken);
  }

  private async toResponseObject(accessToken: string, refreshToken: string): Promise<IAuthResponseData> {
    return { accessToken, refreshToken, expiresIn: this.configService.get<string>('JWT_EXPIRE') };
  }
}

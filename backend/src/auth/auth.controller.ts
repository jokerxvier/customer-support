import { Controller, Post, Body, UseInterceptors, UseGuards, Headers } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { User } from '../shared/entities/pokerdb/user.entity';
import { TransformInterceptor } from 'src/shared/interceptors/transform.interceptor';
import { GetUser } from 'src/shared/decorators/get-user.decorator';
import { IAuthResponseData } from 'src/shared/interfaces/auth.interface';
import { Request } from 'express';

@Controller('auth')
@UseInterceptors(TransformInterceptor)
export class AuthController {

  constructor(private authService: AuthService) {}

  @Post('/signin')
  signIn(@Body() authCredentialDto: AuthCredentialsDto): Promise<IAuthResponseData> {
    return this.authService.signIn(authCredentialDto);
  }

  @Post('/refresh-token')
  refreshToken(@Headers('authorization') bearer: string) {
    return this.authService.refreshToken(bearer);
  }

  @UseGuards(AuthGuard())
  @Post('/me')
  me(@GetUser() user: User) {
    return user;
  }
}

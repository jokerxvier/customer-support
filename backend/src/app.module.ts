import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import configuration from './config/configuration';
import { DatabaseModule } from './shared/modules/database.module';
import { TransactionModule } from './transaction/transaction.module';
import { TableModule } from './table/table.module';
import { TorModule } from './tor/tor.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration]
    }),    
    DatabaseModule,
    AuthModule,
    UserModule,
    TransactionModule,
    TableModule,
    TorModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

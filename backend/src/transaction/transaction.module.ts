import { Module } from '@nestjs/common';
import 'dotenv/config';
import { ConfigService } from '@nestjs/config';
import { AuthModule } from 'src/auth/auth.module';
import { TransactionController } from './transaction.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MoneyTransactionRepository } from 'src/shared/repositories/money-transaction.repository';
import { TransactionService } from './transaction.service';
import { TapiExternalTransactionRepository } from 'src/shared/repositories/tapi-external-transaction';
import { TapiExternalTransaction } from 'src/shared/entities/poker-webadmin/tapi-external-transaction.entity';
import { MoneyTransaction } from 'src/shared/entities/pokerdb/money-transaction.entity';
import { UserRepository } from 'src/shared/repositories/user.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      MoneyTransactionRepository,
    ], process.env.DB_POKER_CONNECTION_NAME),
    TypeOrmModule.forFeature([
      TapiExternalTransactionRepository,
    ], process.env.DB_WEBADMIN_CONNECTION_NAME),
    TypeOrmModule.forFeature([TapiExternalTransaction], process.env.DB_WEBADMIN_CONNECTION_NAME),
    TypeOrmModule.forFeature([MoneyTransaction], process.env.DB_POKER_CONNECTION_NAME),
    AuthModule,
  ],
  controllers: [TransactionController],
  providers: [
    MoneyTransactionRepository,
    UserRepository,
    TapiExternalTransactionRepository,
    TransactionService
  ]
})
export class TransactionModule { }

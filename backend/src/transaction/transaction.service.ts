import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MoneyTransactionRepository } from 'src/shared/repositories/money-transaction.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { TapiExternalTransactionRepository } from 'src/shared/repositories/tapi-external-transaction';

import { UserRepository } from 'src/shared/repositories/user.repository';
import { ITransactionResponseData } from 'src/shared/interfaces/transaction.interface';

@Injectable()
export class TransactionService {
  constructor(private configService: ConfigService,
    @InjectRepository(MoneyTransactionRepository, process.env.DB_POKER_CONNECTION_NAME)
    private moneyTransactionRepository: MoneyTransactionRepository,
    @InjectRepository(TapiExternalTransactionRepository, process.env.DB_WEBADMIN_CONNECTION_NAME)
    private tapiExternalTransactionRepository: TapiExternalTransactionRepository,
    @InjectRepository(UserRepository, process.env.DB_POKER_CONNECTION_NAME)
    private userRepository: UserRepository,
  ) {

  }

  async getLastTransactions(transactionType: number, status: string = ''): Promise<ITransactionResponseData[]> {
    const transactions = await this.moneyTransactionRepository.getLastTransactions(transactionType, status);
    const result = transactions.map(async item => {
      const user = await this.userRepository.findOne({ id: item.playerId });
      const { externalTransactionId, amount, state, moneyTx, txStatus } = item;
      return {
        transactionId: (externalTransactionId) ? externalTransactionId : moneyTx,
        moneyTx: moneyTx,
        status: this.formatStatus(txStatus),
        amount: this.formatNumber(amount),
        moneyType: this.formatCurrency(item.moneyType),
        username: user.username,
        createdAt: item.fStamp,
        finishAt: item.finishTime,
        state: ''
      }
    });

    return await this.toResponseObject(result);
  }

  async getDeposits(transactionType: number) {
    const transactions = await this.moneyTransactionRepository.getDeposits(transactionType);
    const result = transactions.map(async item => {
      const user = await this.userRepository.findOne({ id: item.playerId });
      const { externalTransactionId, amount, state } = item;
      return {
        transactionId: externalTransactionId,
        amount: this.formatNumber(amount),
        moneyType: this.formatCurrency(item.moneyType),
        username: (user) ? user.username : 'User does not exist in database',
        createdAt: item.fStamp,
        state: state
      }
    });

    return await this.toResponseObject(result);
  }

  private async toResponseObject(result): Promise<any> {
    return await Promise.all(result).then(values => values.filter(v => v));
  }

  private formatNumber(amount): string {
    const formatter = new Intl.NumberFormat('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });

    return formatter.format(Math.abs((amount / 100)));
  }

  private formatCurrency(moneyType): string {
    switch (moneyType) {
      case "R":
        return "BTC";
        break;
      case "f":
        return "BCH";
        break;
      default:
        return "";
    }
  }

  private formatStatus(txStatus): string {
    switch (txStatus) {
      case 4:
        return "pending";
        break;
      case 2:
        return "declined";
        break;
      case 0:
        return "approved";
        break;
      default:
        return "";
    }
  }
}

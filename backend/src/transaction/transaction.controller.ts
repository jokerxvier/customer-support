import { Controller, UseGuards, UseInterceptors, Get, Req } from '@nestjs/common';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { TransformInterceptor } from 'src/shared/interceptors/transform.interceptor';
import { ConfigService } from '@nestjs/config';
import { MoneyTransaction } from 'src/shared/entities/pokerdb/money-transaction.entity';
import { TransactionService } from './transaction.service';

@Controller('transaction')
@UseGuards(AuthGuard())
@UseInterceptors(TransformInterceptor)
export class TransactionController {
  constructor(private configService: ConfigService, private transactionService: TransactionService) {

  }

  @Get('/latest-withdrawals')
  getLatestWithdrawal(@Req() req: Request): Promise<any> {
    const status = (req.query.status) ? req.query.status : '';
    return this.transactionService.getLastTransactions(this.configService.get<number>('transaction.withdrawalId'), status);
  }

  @Get('/latest-deposits')
  getLatestDeposits(): Promise<any> {
    return this.transactionService.getDeposits(this.configService.get<number>('transaction.depositId'));
  }
}

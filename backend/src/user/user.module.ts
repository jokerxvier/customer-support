import { Module } from '@nestjs/common';
import 'dotenv/config';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../shared/repositories/user.repository';
import { MoneyTransactionRepository } from '../shared/repositories/money-transaction.repository';
import { TapiExternalTransactionRepository } from 'src/shared/repositories/tapi-external-transaction';
import { TournamentMemberRepository } from 'src/shared/repositories/tournament-member.repository';
import { TournamentRepository } from 'src/shared/repositories/tournament.repository';
import { AffiliatePlayerLogRepository } from 'src/shared/repositories/affiliate-player-log.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      MoneyTransactionRepository,
      TournamentRepository
    ], process.env.DB_POKER_CONNECTION_NAME),
    TypeOrmModule.forFeature([
      TapiExternalTransactionRepository,
      TournamentMemberRepository,
      AffiliatePlayerLogRepository,
    ], process.env.DB_WEBADMIN_CONNECTION_NAME),
    AuthModule
  ],
  controllers: [UserController],
  providers: [
    UserService,
    UserRepository,
    MoneyTransactionRepository,
    TournamentRepository,
    TournamentMemberRepository,
    TapiExternalTransactionRepository,
    TournamentMemberRepository
  ]
})
export class UserModule { }


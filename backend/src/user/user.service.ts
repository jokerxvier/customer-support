import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import 'dotenv/config';
import _ from 'lodash';

import { UserRepository } from '../shared/repositories/user.repository';
import { User } from 'src/shared/entities/pokerdb/user.entity';
import { MoneyTransactionRepository } from 'src/shared/repositories/money-transaction.repository';
import { ConfigService } from '@nestjs/config';
import { TapiExternalTransactionRepository } from 'src/shared/repositories/tapi-external-transaction';
import { ITransactionResponseData, IP2PTransactionResponeData } from 'src/shared/interfaces/transaction.interface';
import { TournamentRepository } from 'src/shared/repositories/tournament.repository';
import { TournamentMemberRepository } from 'src/shared/repositories/tournament-member.repository';
import { ITournamentResponseData } from 'src/shared/interfaces/tournament.interface';
import { AffiliatePlayerLogRepository } from 'src/shared/repositories/affiliate-player-log.repository';

@Injectable()
export class UserService {
  constructor(
    private configService: ConfigService,
    @InjectRepository(UserRepository, process.env.DB_POKER_CONNECTION_NAME)
    private userRepository: UserRepository,

    @InjectRepository(MoneyTransactionRepository, process.env.DB_POKER_CONNECTION_NAME)
    private moneyTransactionRepository: MoneyTransactionRepository,

    @InjectRepository(TournamentRepository, process.env.DB_POKER_CONNECTION_NAME)
    private tournamentRepository: TournamentRepository,

    @InjectRepository(TournamentMemberRepository, process.env.DB_WEBADMIN_CONNECTION_NAME)
    private tournamentMemberRepository: TournamentMemberRepository,

    @InjectRepository(TapiExternalTransactionRepository, process.env.DB_WEBADMIN_CONNECTION_NAME)
    private tapiExternalTransactionRepository: TapiExternalTransactionRepository,

    @InjectRepository(AffiliatePlayerLogRepository, process.env.DB_WEBADMIN_CONNECTION_NAME)
    private affilitePlayerLogRepository: AffiliatePlayerLogRepository,

  ) { }

  async getByUsername(username: string): Promise<User> {
    const user = await this.userRepository.findOne({ username });
    if (!user) {
      throw new NotFoundException(`Username with ${username} not found!`)
    }
    return user;
  }

  async getUserLastTransactions(username: string, transactionType: number, status: string = ''): Promise<ITransactionResponseData[]> {
    const user = await this.getByUsername(username);
    const transactions = await this.moneyTransactionRepository.getUserLastTransactions(user, transactionType, status);

    const result = transactions.map(async item => {
      const { externalTransactionId, amount, state, moneyTx, txStatus } = item;
      const user = await this.userRepository.findOne({ id: item.playerId });
      return {
        transactionId: (externalTransactionId) ? externalTransactionId : moneyTx,
        moneyTx: moneyTx ? moneyTx : '',
        status: this.formatStatus(txStatus),
        moneyType: this.formatCurrency(item.moneyType),
        amount: this.formatNumber(amount),
        username: user.username,
        createdAt: item.fStamp,
        finishAt: item.finishTime,
        state: ''
      }
    });

    return await this.toResponseObject(result);
  }

  async getDeposits(username: string, transactionType: number): Promise<ITransactionResponseData[]> {
    const user = await this.getByUsername(username);
    const transactions = await this.moneyTransactionRepository.getDeposits(transactionType, user);
    const result = transactions.map(async item => {
      const user = await this.userRepository.findOne({ id: item.playerId });
      const { externalTransactionId, amount, state } = item;
      return {
        transactionId: externalTransactionId,
        amount: this.formatNumber(amount),
        moneyType: this.formatCurrency(item.moneyType),
        username: (user) ? user.username : 'User does not exist in database',
        createdAt: item.fStamp,
        state: state
      }
    });

    return await this.toResponseObject(result);
  }

  async getLatestP2PTransaction(username: string): Promise<IP2PTransactionResponeData[]> {
    const user = await this.getByUsername(username);
    const p2pTransactions = await this.moneyTransactionRepository.getSenderLastP2PTransactions(user, this.configService.get<number>('transaction.p2pSendId'));

    const result = p2pTransactions.map(async senderTransaction => {
      const receiver = await this.moneyTransactionRepository.getReceiverTransaction(senderTransaction, this.configService.get<number>('transaction.p2pRecievedId'));

      const fromUser = await this.userRepository.findOne({ id: senderTransaction.playerId });

      let receiverUsername = '';
      let receiverId: any;

      if (receiver) {
        const toUser = await this.userRepository.findOne({ id: receiver.playerId });
        receiverUsername = toUser.username;
        receiverId = receiver.id;
      } else {
        receiverUsername = 'receiver data does not exist';
        receiverId = 'receiver data does not exist';
      }

      return {
        fromTransactionId: senderTransaction.id,
        moneyType: this.formatCurrency(senderTransaction.fMoneyType),
        fromUser: fromUser.username,
        toTransactionId: receiverId,
        toUser: receiverUsername,
        amount: this.formatNumber(senderTransaction.fValue),
        createdAt: senderTransaction.fStamp
      }
    });

    return await this.toResponseObject(result);
  }

  async getUserJoinedTournaments(username: string): Promise<ITournamentResponseData[]> {
    const user = await this.getByUsername(username);
    const joinedTournaments = await this.tournamentMemberRepository.getUserLastJoinTournament(user);

    const result = joinedTournaments.map(async joinedTournaments => {
      const tournament = await this.tournamentRepository.findOne({ id: joinedTournaments.fTournamentId });
      const data: ITournamentResponseData = {
        tournamentId: joinedTournaments.fTournamentId,
        prize: Number(joinedTournaments.fPrize),
        name: (tournament) ? tournament.fName : 'tournament data does not exist anymore',
        buyIn: Number(joinedTournaments.fBuyIn),
        joinedAt: joinedTournaments.fStamp,
      }
      return data;
    });

    return await this.toResponseObject(result);
  }

  async getRake(username: string, rakeType: string): Promise<any> {
    const user = await this.getByUsername(username);
    let result = [];

    if (rakeType === 'daily') {
      const rakes = await this.moneyTransactionRepository.getUserDailyTransactionRake(user, this.configService.get<number>('transaction.rakeId'));
      result = rakes.map(async res => {
        const data = {
          'date': res.date,
          'rake': Number(res.rake).toFixed(2)
        }
        return data;
      });

      return await this.toResponseObject(result);
    }

    if (rakeType === 'lifetime') {
      const rake = await this.moneyTransactionRepository.getUserLifetimeTransactionRake(user, this.configService.get<number>('transaction.rakeId'));
      const data = {
        'date': rake.date,
        'rake': Number(rake.rake).toFixed(2)
      }

      return data;
    }

    return [];
  }

  async getLatestPlayerTagByAffiliate(): Promise<any> {
    const affiliatePlayerLog = await this.affilitePlayerLogRepository.getLatestPlayerTagByAffiliate();
    const result = affiliatePlayerLog.map(async playerLog => {
      const player = await await this.userRepository.findOne({ id: playerLog.fPlayerId });

      return {
        username: player.username,
        createdAt: playerLog.fCreatedAt
      };
    });

    return await this.toResponseObject(result);
  }

  private async toResponseObject(result): Promise<any> {
    return await Promise.all(result).then(values => values.filter(v => v));
  }

  private formatNumber(amount): string {
    const formatter = new Intl.NumberFormat('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    });

    return formatter.format(Math.abs((amount / 100)));
  }

  private formatCurrency(moneyType): string {
    switch (moneyType) {
      case "R":
        return "BTC";
        break;
      case "f":
        return "BCH";
        break;
      default:
        return "";
    }
  }

  private formatStatus(txStatus): string {
    switch (txStatus) {
      case 4:
        return "pending";
        break;
      case 2:
        return "declined";
        break;
      case 0:
        return "approved";
        break;
      default:
        return "";
    }
  }

}

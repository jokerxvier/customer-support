import { Controller, Param, Get, UseGuards, UseInterceptors, Req } from '@nestjs/common';
import { Request } from 'express';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { TransformInterceptor } from 'src/shared/interceptors/transform.interceptor';
import { User } from 'src/shared/entities/pokerdb/user.entity';
import { ConfigService } from '@nestjs/config';
import { ITransactionResponseData, IP2PTransactionResponeData } from 'src/shared/interfaces/transaction.interface';
import { ITournamentResponseData } from 'src/shared/interfaces/tournament.interface';


@Controller('users')
@UseGuards(AuthGuard())
@UseInterceptors(TransformInterceptor)
export class UserController {

  constructor(private userService: UserService, private configService: ConfigService) { }

  @Get('/latest-tagged-by-affiliate')
  getLatestPlayerTagByAffiliate(): Promise<any> {
    return this.userService.getLatestPlayerTagByAffiliate();
  }


  @Get('/:username')
  getByUsername(@Param('username') username: string): Promise<User> {
    return this.userService.getByUsername(username);
  }

  @Get('/:username/deposits')
  getDeposits(@Param('username') username: string): Promise<ITransactionResponseData[]> {
    return this.userService.getDeposits(username, this.configService.get<number>('transaction.depositId'));
  }

  @Get('/:username/withdrawals')
  getWithdrawals(@Param('username') username: string, @Req() req: Request): Promise<ITransactionResponseData[]> {
    const status = (req.query.status) ? req.query.status : '';
    return this.userService.getUserLastTransactions(username, this.configService.get<number>('transaction.withdrawalId'), status);
  }

  @Get('/:username/latestP2Ptransfer')
  getLastestTransfer(@Param('username') username: string): Promise<IP2PTransactionResponeData[]> {
    return this.userService.getLatestP2PTransaction(username);
  }

  @Get('/:username/joined-tournaments')
  getJoinedTournaments(@Param('username') username: string): Promise<ITournamentResponseData[]> {
    return this.userService.getUserJoinedTournaments(username);
  }

  @Get('/:username/rake/daily')
  getDailyRake(@Param('username') username: string): Promise<any> {
    return this.userService.getRake(username, 'daily');
  }

  @Get('/:username/rake/lifetime')
  getLifetimeRake(@Param('username') username: string): Promise<any> {
    return this.userService.getRake(username, 'lifetime');
  }
}

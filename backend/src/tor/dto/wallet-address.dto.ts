import { IsString, MaxLength, MinLength, IsIn, IsNotEmpty } from 'class-validator';

export class WalletAddressDto {
    @IsString()
    @IsIn(["BCH", "BTC"])
    type: string;

    @IsString()
    @IsNotEmpty()
    address: string;
}
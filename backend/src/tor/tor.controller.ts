import { Controller, Req, Get, Param } from '@nestjs/common';
import { WalletAddressDto } from './dto/wallet-address.dto';
import { Request } from 'express';
import { TorService } from './tor.service';


@Controller('tor')
export class TorController {

  constructor(private torService: TorService) {}

  @Get('/validate/:type/:address')
  validateWalletAddress(@Param() walletAddressDto: WalletAddressDto): Promise<any>{
    return this.torService.validateWalletAddress(walletAddressDto);
  }
}

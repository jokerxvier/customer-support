import { Injectable } from '@nestjs/common';
import { WalletAddressDto } from './dto/wallet-address.dto';
import { torSetup } from 'tor-axios';
import { ConfigService } from '@nestjs/config';
import 'dotenv/config';

@Injectable()
export class TorService {
  protected readonly tor: torSetup;
  constructor(private configService: ConfigService) {
    this.tor = torSetup({
      port: this.configService.get<string>('TOR_PORT'),
      ip: this.configService.get<string>('TOR_HOST'),
      path: '',
      controlPort: this.configService.get<string>('TOR_CONTROL_PORT'),
      controlPassword: this.configService.get<string>('TOR_CONTROL_PASSWORD')
    });
  }

  async validateWalletAddress(walletAddressDto: WalletAddressDto) {
    const {type, address} = walletAddressDto;
    try {
      let response = await this.tor.get(`${this.configService.get<string>('TOR_WALLET_HOST')}/address/${type}/${address}/validate`);
      return response.data;
    } catch (error) {
      if (typeof error.message === 'string' && error.message.includes("Proxy connection timed out")) await this.tor.torNewSession();
      throw error;
    }
  }
}

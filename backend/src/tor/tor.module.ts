import { Module } from '@nestjs/common';
import { TorController } from './tor.controller';
import { TorService } from './tor.service';

@Module({
  controllers: [TorController],
  providers: [TorService]
})
export class TorModule {}
